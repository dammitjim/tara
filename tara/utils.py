def get_filename_from_url(url: str) -> str:
    parts = url.split("/")
    name = parts[-1]
    if "?" in name:
        name = name.replace("?", "_")
    return name
