import peewee


db = peewee.SqliteDatabase("bookmarks/bookmarks.db")


class Bookmark(peewee.Model):
    title = peewee.CharField()
    url = peewee.CharField()

    class Meta:
        database = db


def initialise_database():
    db.connect()
    db.create_tables([Bookmark])
    return db
