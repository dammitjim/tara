from dataclasses import dataclass

import peewee
import toml

from tara.database import initialise_database


@dataclass
class UserConfig:
    # TODO: change these default values before distribution
    config_file_path = "./example-config.toml"
    dir_bookmarks = "./bookmarks"

    def load(self):
        try:
            with open(self.config_file_path, "r") as f:
                parsed_config = toml.load(f)
                self._extract_locations(parsed_config)
        except FileNotFoundError:
            # TODO: log, ensure that sensible defaults are set
            pass

    def _extract_locations(self, config: dict):
        if "locations" not in config:
            return

        if "bookmarks" in config["locations"]:
            self.dir_bookmarks = config["locations"]["bookmarks"]


@dataclass
class TaraConfig:
    database: peewee.SqliteDatabase
    user_config = UserConfig()


def initialise_config() -> TaraConfig:
    db = initialise_database()
    config = TaraConfig(database=db)
    config.user_config.load()
    return config
