import os

import frontmatter
from requests_html import HTMLSession
from slugify import Slugify

from tara.config import TaraConfig
from tara.database import Bookmark

_Folderpath = str

# TODO: handle maximum length
FolderSlugify = Slugify(to_lower=True, separator="_")


class BookmarkManager:
    def __init__(self, tara_config: TaraConfig):
        self.session = HTMLSession()
        self._config = tara_config

    def save(self, url: str, notes="") -> _Folderpath:
        title = self._get_bookmark_title(url)
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        folder = FolderSlugify(url)
        folder_path = os.path.join(base_dir, "bookmarks", folder)
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

        db_obj = self._save_to_db(title, url)
        plain_text = self._generate_post_plaintext(notes, url=url, title=title, pk=db_obj.id)
        filepath = os.path.join(folder_path, "_main.md")
        with open(filepath, "w") as post_file:
            post_file.write(plain_text)

        self._save_to_db(title, url)

        return folder_path

    def _get_bookmark_title(self, url) -> str:
        # TODO log failure to connect
        response = self.session.get(url)
        title = response.html.xpath("//title", first=True)
        title = title.text if title else url
        return title

    def _generate_post_plaintext(self, notes, **kwargs):
        post = frontmatter.Post(notes, **kwargs)
        return frontmatter.dumps(post)

    def _save_to_db(self, title, url) -> Bookmark:
        db_bookmark = Bookmark(title=title, url=url)
        db_bookmark.save()
        return db_bookmark
