import asyncio
import collections
import functools
import os
from typing import List

from bs4 import BeautifulSoup
import requests
from requests_html import HTMLSession

from tara.utils import get_filename_from_url


URLMap = collections.namedtuple("URLMap", ["old", "new"])


class SiteDownloader:
    def __init__(self, url: str, folder: str):
        self.url = url
        self.folder = folder
        self.session = HTMLSession()
        self.loop = asyncio.get_event_loop()

    def download(self):
        self._run()

    def _run(self):
        self.response = self.session.get(self.url)
        css_links = self.response.html.xpath("//link[@rel='stylesheet']/@href")
        self.url_map = self.loop.run_until_complete(self._download_urls(css_links))
        self._write_html()

    def _write_html(self):
        file_path = os.path.join(self.folder, "archive", "index.html")

        html = self.response.html.raw_html.decode("utf-8")
        soup = BeautifulSoup(html, features="lxml")
        [s.extract() for s in soup("script")]
        html = str(soup)
        for map in self.url_map:
            html = html.replace(map.old, map.new)

        with open(file_path, "w") as file:
            file.write(html)

    async def _download_url(self, rel_url) -> URLMap:
        url = self.response.html._make_absolute(rel_url)
        filename = get_filename_from_url(url)

        requests_fn = functools.partial(requests.get, url, {"stream": True})
        response = await self.loop.run_in_executor(None, requests_fn)

        archive_folder_path = os.path.join(self.folder, "archive")
        if not os.path.exists(archive_folder_path):
            os.makedirs(archive_folder_path)

        file_path = os.path.join(archive_folder_path, filename)
        with open(file_path, "wb") as f:
            for chunk in response:
                f.write(chunk)

        return URLMap(old=rel_url, new=f"./{filename}")

    async def _download_urls(self, urls: List[str]) -> List[URLMap]:
        futures = [self._download_url(link) for link in urls]
        urls = await asyncio.gather(*futures)
        return urls
