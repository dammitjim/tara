from typing import List

import click

from tara.bookmark import BookmarkManager
from tara.config import initialise_config
from tara.pull import SiteDownloader


@click.argument("notes", nargs=-1)
@click.argument("url", nargs=1)
@click.option("--archive")
@click.command()
@click.pass_context
def new_bookmark(ctx: click.Context, url: str, notes: List[str], archive: bool):
    """Create a new bookmark.

    Should follow the syntax:

    tara new https://tightenupthe.tech some guys tech website #tech
    """
    manager = BookmarkManager(ctx.obj["config"])
    folder = manager.save(url, " ".join(notes))
    if archive:
        downloader = SiteDownloader(url, folder)
        downloader.download()


@click.command()
@click.pass_context
def list_bookmarks(ctx: click.Context):
    """List existing bookmarks."""
    print(ctx.obj["config"])


@click.command()
def delete_bookmark():
    """Delete a bookmark by ID."""
    pass


@click.command()
def find_bookmark():
    """Find stored bookmarks."""
    pass


@click.group()
@click.pass_context
def tara(ctx: click.Context):
    ctx.ensure_object(dict)
    config = initialise_config()
    ctx.obj["config"] = config


tara.add_command(new_bookmark, name="new")
tara.add_command(list_bookmarks, name="list")
tara.add_command(delete_bookmark, name="delete")
tara.add_command(find_bookmark, name="find")
