# Tara

Liberated bookmarks.

Currently very early stages, nowhere near production ready and 
the code is mostly a dumping ground of ideas.

## Planned Syntax

Create a new bookmark

```bash
tara new https://tightenupthe.tech some guys tech website
```

Tag a bookmark under #tech and the nested #personal/websites

```bash
tara new https://tightenupthe.tech some guys tech website #tech #personal/websites
```

List bookmarks

```bash
tara list
```

List bookmarks under tag
```bash
tara list #tech
```

Delete bookmarks with IDs 1 2 and 3

```bash
tara delete 1 2 3
```

Find a bookmark with phrase "tighten" within tag "#personal" or "#tech"

```bash
tara find tighten #personal #tech
```